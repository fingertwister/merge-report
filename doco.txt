The merge-report is a little utility I wrote when working with teams in a subversion repository that needed to manage their merges back to mainline trunk from multiple development and production hot fix branches. The basic idea was to help people merge early and often by constructing an email report of all outstanding merges and sending it to only those team members that had outstanding merges. If people kept on top of their merges the emails went away, and as the development manager I had direct insight (cc'd on every report) as to the risk from unmerged changes we were carrying as a whole.

To make it even easier, as I was constructing up the report I realised I could provide the command for each member that would merge their changes back to the "merge-to" branch, giving them simple copy-and-paste merge functionality, assuming they didn't have any conflicts.

At the moment, if you take the compiled zip file and extract it to your run location you will need to do some simple set up tasks to make the merge report work for you. These are:

Check out a local "clean" copy of the "merge to" repositories.
Update the run scripts to perform an svn update prior to running the report to pick up any merges that have happened since the last time the report ran
Update the JVM arguments to include the mail host and domain for email addresses
Edit the spring configuration file to include your repository configuration(s) to drive your report. The merge report will quite happily run multiple configurations in the one report
Run a test report to verify everything is working
Schedule the report with a windows scheduled task or cron job
I should point out I've generally only run the report from my workstation which has been windows every time I've used the report, so *nix style scripts are not included in the package (yet), but should be easy enough to write.

The report assumes all merges are being run with the subversion merge utility (prior was svn-merge python utility) and provides basic how-to information in the email as well for those new to merging.

If you have any feedback, patches or enhancement requests I'd love to hear about them. I've not had much experience with git and I'm guessing it would be equally possible to create a similar report for git repositories, although I'm guessing git users are generally more savvy with managing merges given the excellent built in support for merging from the ground up.
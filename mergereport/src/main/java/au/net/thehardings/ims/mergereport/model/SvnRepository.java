package au.net.thehardings.ims.mergereport.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The class <code>Repository</code>
 */
public class SvnRepository extends RepositoryBase {
    private static final Logger LOG = LoggerFactory.getLogger(SvnRepository.class);

    public SvnRepository() {
    }

    public SvnRepository(String branchName, String base, String name, String url) {
        super(branchName, base, name, url);
    }

    public List<Commit> getUnmergedCommits() {
        LOG.info("Getting unmerged commits from the branch");
        String command = "svn mergeinfo --show-revs eligible " + getBranchUrl();
        LOG.debug(command);
        Scanner scanner = processor.runProcess(command, getBase());
        List<Integer> commitIds = new ArrayList<Integer>();
        while (scanner.hasNextLine()) {
            String commitDetail = scanner.nextLine().trim().replace("r", "");
            if (commitDetail.equals("")) {
                continue;
            }
            commitIds.add(Integer.valueOf(commitDetail));
        }
        LOG.info("Found " + commitIds.size() + " commits that have not been merged.");

        LOG.info("Retrieving the detail for each commit...");
        List<Commit> outstanding = new ArrayList<Commit>();
        for (Integer commitId : commitIds) {
            outstanding.add(getCommit(commitId));
        }
        return outstanding;
    }

    Commit getCommit(Integer commitId) {
        Scanner scanner = processor.runProcess("svn log " + getUrl() + " -r " + commitId, getBase());
        scanner.nextLine(); //header
        String[] details = scanner.nextLine().split(" \\| ");
        scanner.nextLine(); //blank
        List<String> comment = new ArrayList<String>();
        while (comment.size() == 0 || !"------------------------------------------------------------------------".equals(comment.get(comment.size() - 1))) {
            comment.add(scanner.nextLine());
        }
        comment.remove(comment.size() - 1); //the last line is the footer
        return new Commit.Builder(this, getBranchName(), commitId).username(details[1].trim()).date(details[2].trim()).comment(comment).build();
    }

}

package au.net.thehardings.ims.mergereport;

import au.net.thehardings.ims.mergereport.dispatch.Dispatcher;
import au.net.thehardings.ims.mergereport.dispatch.EmailDispatcher;
import au.net.thehardings.ims.mergereport.model.Commit;
import au.net.thehardings.ims.mergereport.model.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The class <code>Test</code>
 */
public class MergeReport {
    /**
     * Logger for this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(MergeReport.class);

    static String contextFile;
    static String requestorAddress;
    static String mailHost;
    static String mailDomain;
    static boolean notifyAll = false;
    static PrintStream out = System.out;

    Dispatcher dispatcher = new EmailDispatcher();
    List<Commit> outstanding = new ArrayList<Commit>();
    List<Repository> repositories;

    public static void main(String[] args) {
        try {
            LOG.info("*****************************************");
            LOG.info("**            Merge Report             **");
            LOG.info("*****************************************");

            parseArgs(args);

            ApplicationContext context = new ClassPathXmlApplicationContext(contextFile);
            MergeReport mergeReport = (MergeReport) context.getBean("mergeReport");
            mergeReport.run();
        } catch (Throwable e) {
            LOG.error("An exception occurred during processing.", e);
        }
    }

    public static String getRequestorAddress() {
        return requestorAddress;
    }

    public static void setRequestorAddress(String requestorAddress) {
        MergeReport.requestorAddress = requestorAddress;
    }

    public static String getMailHost() {
        return mailHost;
    }

    public static void setMailHost(String mailHost) {
        MergeReport.mailHost = mailHost;
    }

    public static String getMailDomain() {
        return mailDomain;
    }

    public static void setMailDomain(String mailDomain) {
        MergeReport.mailDomain = "@" + mailDomain;
    }

    public static boolean isNotifyAll() {
        return notifyAll;
    }

    public static void setNotifyAll(boolean notifyAll) {
        MergeReport.notifyAll = notifyAll;
    }

    static void parseArgs(String[] args) {
        setMailHost(System.getProperty("mail.host"));
        if (mailHost == null || "".equals(mailHost)) {
            showUsage();
        }

        setMailDomain(System.getProperty("mail.domain"));
        if ("@null".equals(mailDomain) || "@".equals(mailDomain)) {
            showUsage();
        }

        List<String> params = Arrays.asList(args);

        if (params.indexOf("help") > -1) {
            showUsage();
        }
        if (params.indexOf("?") > -1) {
            showUsage();
        }
        if (params.indexOf("-?") > -1) {
            showUsage();
        }
        if (params.indexOf("-help") > -1) {
            showUsage();
        }

        if (params.indexOf("-e") > -1) {
            if (params.size() > params.indexOf("-e") + 1) {
                requestorAddress = params.get(params.indexOf("-e") + 1);
            }
        } else {
            requestorAddress = System.getProperty("user.name") + mailDomain;
        }

        if (requestorAddress == null || "".equals(requestorAddress)) {
            showUsage();
        }

        if (params.indexOf("-f") > -1) {
            if (params.size() > params.indexOf("-f") + 1) {
                contextFile = params.get(params.indexOf("-f") + 1);
            }
        } else {
            contextFile = "merge-report.xml";
        }

        setNotifyAll(params.indexOf("-a") > -1);

        if (contextFile == null || "".equals(contextFile)) {
            showUsage();
        }
    }

    static void showUsage() {
        out.println("Usage: MergeReport [-f <context file>] [-e <email addresses>] [-a] [-auto]");
        out.println(" The system properties mail.host and mail.domain must be set.");
        out.println(" Options:");
        out.println("  -f <context file>: the spring context file for the report (default merge-report.xml)");
        out.println("  -e <email addresses>: comma separated email to always send report to (default system user)");
        out.println("  -a: send report to users that have outstanding merges");
    }

    public void run() throws IOException, InterruptedException, MessagingException {
        for (Repository repository : repositories) {
            outstanding.addAll(repository.getUnmergedCommits());
        }

        LOG.info("Sending the report email");
        dispatcher.dispatch(outstanding);
    }

    List<Integer> getCommitIds(String commitDetail) throws IOException, InterruptedException {
        List<Integer> commitIds = new ArrayList<Integer>();

        if (commitDetail == null || "".equals(commitDetail)) {
            return commitIds;
        }

        String[] commits = commitDetail.split(",");

        for (String commit : commits) {
            if (commit.indexOf(":") > -1) {
                String[] range = commit.split(":");
                int start = Integer.parseInt(range[0]);
                int end = Integer.parseInt(range[1]);
                for (; start <= end; start++) {
                    commitIds.add(start);
                }
            } else {
                commitIds.add(Integer.valueOf(commit));
            }
        }

        return commitIds;
    }

    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}

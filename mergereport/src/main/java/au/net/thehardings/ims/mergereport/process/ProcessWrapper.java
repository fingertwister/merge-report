package au.net.thehardings.ims.mergereport.process;

import java.io.File;
import java.util.Scanner;

/**
 * The class <code>ProcessWrapper</code>
 */
public interface ProcessWrapper {
    Scanner runProcess(String command, File dir);
}

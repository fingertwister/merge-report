package au.net.thehardings.ims.mergereport.model;

import java.util.List;

public interface Repository {
    List<Commit> getUnmergedCommits();

    String getBranchName();

    String getBranchUrl();

    String getName();

    String getUrl();
}

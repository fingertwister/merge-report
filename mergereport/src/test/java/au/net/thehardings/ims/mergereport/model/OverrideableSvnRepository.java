package au.net.thehardings.ims.mergereport.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OverrideableSvnRepository extends SvnRepository {
    public boolean overrideGetUnmergedCommits = false;
    public int getUnmergedCommitsInvocationCount = 0;
    public boolean overrideGetCommit = false;

    public OverrideableSvnRepository() {
    }

    public OverrideableSvnRepository(String branchName, String base, String name, String url) {
        super(branchName, base, name, url);
    }

    @Override
    public List<Commit> getUnmergedCommits() {
        if (overrideGetUnmergedCommits) {
            getUnmergedCommitsInvocationCount++;
        } else {
            return super.getUnmergedCommits();
        }
        return Collections.emptyList();
    }

    @Override
    Commit getCommit(Integer commitId) {
        if (overrideGetCommit) {
            return new Commit.Builder(this, this.getBranchName(), commitId).username("user").date("date").comment(new ArrayList<String>()).build();
        } else {
            return super.getCommit(commitId);
        }
    }
}
package au.net.thehardings.ims.mergereport.model;

import au.net.thehardings.ims.mergereport.AllTests;
import au.net.thehardings.ims.mergereport.process.ProcessWrapper;
import org.jmock.Mock;

import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Scanner;

/**
 * The class <code>RepositoryTest</code>
 */
public class SvnRepositoryTest extends AllTests {
    OverrideableSvnRepository repository;

    /**
     * Test lifecycle method runs before each test case.
     *
     * @throws Exception if an error occurs during test
     */
    protected void setUp() throws Exception {
        super.setUp();
        repository = new OverrideableSvnRepository("branch-name", "trunk", "repositoryName", "repositoryUrl");
    }

    /**
     * Test case for the <code>getUnmergedCommits()</code> method
     * of class <code>MergeReport</code>
     *
     * @throws Exception if an error occurs during test
     */
    public void testGetUnmergedCommits() throws Exception {
        StringBuffer commitLog = new StringBuffer();
        commitLog.append("r519\nr550\nr554\nr567\n");
        commitLog.append("\n");
        Scanner scanner = new Scanner(new ByteArrayInputStream(commitLog.toString().getBytes()));

        Mock processorMock = mock(ProcessWrapper.class);
        processorMock.expects(once()).method("runProcess").with(eq("svn mergeinfo --show-revs eligible " + repository.getUrl() + "/branches/" + repository.getBranchName()), eq(repository.getBase())).will(returnValue(scanner));
        repository.processor = (ProcessWrapper) processorMock.proxy();

        repository.overrideGetCommit = true;
        List<Commit> commits = repository.getUnmergedCommits();
        assertTrue("Commit id's not parsed successfully.", commits.size() == 4);
        assertEquals("Commit id's not parsed successfully.", "519", commits.get(0).getCommitId());
        assertEquals("Commit id's not parsed successfully.", "550", commits.get(1).getCommitId());
        assertEquals("Commit id's not parsed successfully.", "554", commits.get(2).getCommitId());
        assertEquals("Commit id's not parsed successfully.", "567", commits.get(3).getCommitId());
    }

    /**
     * Test case for the <code>getCommit()</code> method
     * of class <code>MergeReport</code>
     *
     * @throws Exception if an error occurs during test
     */
    public void testGetCommit() throws Exception {
        int commitId = 100;
        StringBuffer commitLog = new StringBuffer();
        commitLog.append("------------------------------------------------------------------------").append("\n");
        commitLog.append("r100 | user | 2007-09-21 12:12:52 +0900 (Fri, 21 Sep 2007) | 3 lines").append("\n");
        commitLog.append("\n");
        commitLog.append("comment line 1").append("\n");
        commitLog.append("comment line 2").append("\n");
        commitLog.append("comment line 3").append("\n");
        commitLog.append("------------------------------------------------------------------------").append("\n");
        commitLog.append("\n");
        Scanner scanner = new Scanner(new ByteArrayInputStream(commitLog.toString().getBytes()));

        Mock processorMock = mock(ProcessWrapper.class);
        processorMock.expects(once()).method("runProcess").with(eq("svn log " + repository.getUrl() + " -r " + commitId), eq(repository.getBase())).will(returnValue(scanner));
        repository.processor = (ProcessWrapper) processorMock.proxy();

        Commit commit = repository.getCommit(100);
        assertEquals("branch name was not correct", "branch-name", commit.getBranchName());
        assertTrue("comment was not successfully read", commit.getComment().size() == 3);
        assertEquals("comment 1 was not correct", "comment line 1", commit.getComment().get(0));
        assertEquals("comment 2 was not correct", "comment line 2", commit.getComment().get(1));
        assertEquals("comment 3 was not correct", "comment line 3", commit.getComment().get(2));
        assertEquals("commit id was not correct", Integer.toString(commitId), commit.getCommitId());
        assertEquals("date was not correct", "2007-09-21 12:12:52 +0900 (Fri, 21 Sep 2007)", commit.getDate());
        assertEquals("repository name was not correct", repository, commit.getRepository());
        assertEquals("username was not correct", "user", commit.getUsername());
    }

}